from reportlab.graphics import renderPDF
from svglib.svglib import svg2rlg

input = "trka"
drawing = svg2rlg(input + ".svg")
renderPDF.drawToFile(drawing, input + ".pdf")