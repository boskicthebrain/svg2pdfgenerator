# Svg2PdfGenerator #

This is an experiment. It doesn't work well for all SVG files.

### What is this repository for? ###

This repository makes use of svglib, which had to be ported to Python 3 first, and reportlab.

### How do I get set up? ###

This has been developed in VisualStudio 2013 by using Python Tools for Visual Studio. Solution file included.
In order to get this running create a virtual environment with a Python 3+ interpreter.

### Contribution guidelines ###

Test diferent SVGs until it works consistently like in all modern web browsers.

### Who do I talk to? ###

If you have questions for me: Boško Stupar stupar.bosko@gmail.com